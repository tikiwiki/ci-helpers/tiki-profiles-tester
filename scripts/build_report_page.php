<?php

// Copyright (c) 2016, Avan.Tech, et. al.
// Copyright (c) 2008, Luis Argerich, Garland Foster, Eduardo Polidor, et. al.
// All Rights Reserved. See copyright.txt for details and a complete list of authors.
// Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See license.txt for details.

if (PHP_SAPI !== 'cli') {
    die('Only available through command-line.');
}

$appPath = dirname(__DIR__);
$reportPath = $appPath . '/reports';

// Copy assets to public folder
@mkdir($appPath . '/public/css', 0775, true);
@mkdir($appPath . '/public/js', 0775, true);
@mkdir($appPath . '/public/webfonts', 0775, true);

copy('vendor/twbs/bootstrap/dist/css/bootstrap.min.css', 'public/css/bootstrap.min.css');
copy('vendor/fortawesome/font-awesome/css/fontawesome.min.css', 'public/css/fontawesome.min.css');
copy('vendor/fortawesome/font-awesome/css/solid.min.css', 'public/css/fontawesome-solid.min.css');
copy('vendor/twbs/bootstrap/dist/js/bootstrap.bundle.min.js', 'public/js/bootstrap.bundle.min.js');
copy('vendor/components/jquery/jquery.min.js', 'public/js/jquery.min.js');
exec('cp -r vendor/fortawesome/font-awesome/webfonts/* public/webfonts/');

$paramList = array_diff(scandir($reportPath), array('..', '.'));
$listFiles = [];
unset($paramList[0]);
foreach ($paramList as $paramFile) {
    $reportFile = $reportPath . '/' . $paramFile;
    if (file_exists($reportFile)) {
        $listFiles[] = $reportFile;
    }
}

// Sort the list of files
usort($listFiles, function($a, $b) {
    $regex = '/(all)?_results_([\d].+|master)\.csv$/';
    preg_match($regex, $a, $aMatch);
    preg_match($regex, $b, $bMatch);

    // all will came last
    $aKey = ($aMatch[2] ?? '') . ($aMatch[1] ?? '');
    $bKey = ($bMatch[2] ?? '') . ($bMatch[1] ?? '');

    if ($aKey == $bKey) {
        return 0;
    }

    return ($aKey < $bKey) ? -1 : 1;
});

$profilesFile = file_get_contents($appPath . '/scripts/profiles.txt');
$profiles = explode(PHP_EOL, $profilesFile);
sort($profiles);

$header = [];
$profilesTesterResult = [];
$profileHeader = [
    'Profile'
];
$profileCount = 0;
$index = 0;
foreach ($profiles as $profile) {
    $profileInfo = [];

    // reading files
    $version = '';
    $profilesTesterResult[$profileCount][] = $profile;
    foreach ($listFiles as $file) {
        preg_match('/(all)?_results_([\d].+|master)\.csv$/', $file, $match);
        $version = $match[2] ?? '';
        $version .= ! empty($match[1]) ? ' (all)' : '';
        $delimiter = ',';
        $profileHeader[] = $version;

        // reading file content
        $row = 0;
        if (($handle = fopen($file, 'r')) !== false) {
            while (($data = fgetcsv($handle, 10000, $delimiter)) !== false) {
                $row++;
                if ($row == 1) {
                    continue;
                }

                if (!empty($data[5]) && $profile == $data[5]) {
                    $test_date = !empty($data[3]) ? $data[3] : '';
                    $profileName = $data[5];
                    $profileApplied = !empty($data[6]);
                    $nErrors = !empty($data[7]) ? $data[7] : 0;
                    $nWarnings = !empty($data[8]) ? $data[8] : 0;
                    $details = !empty($data[9]) ? $data[9] : '';

                    $cssClass = 'success';
                    $content = '<i class="fas fa-check-circle"></i>';
                    if ($nErrors > 0) {
                        $cssClass = 'error';
                        $content = '<i class="fas fa-times-circle"></i>';
                    } elseif ($nErrors == 0 && $nWarnings > 0) {
                        $cssClass = 'warning';
                        $content = '<i class="fas fa-exclamation-circle"></i>';
                    }
                    $index++;
                    $popover = '';
                    $popoverTitle = 'Details';
                    if ($nErrors || $nWarnings) {
                        $popoverTitle = sprintf('Errors: %d | Warnings: %d', $nErrors, $nWarnings);
                        $popover = 'data-bs-html="true" data-bs-toggle="popover" title="' . $popoverTitle . '" data-bs-trigger="click" data-bs-content="<div><pre>' . htmlentities($details) . '</pre></div>"';
                    }
                    $rowValue = '<td tabindex="'.$index.'" class="' . $cssClass . ' result" ' . $popover . '>';
                    $rowValue .= $content;
                    $rowValue .= '</td>';

                    $profilesTesterResult[$profileCount][] = $rowValue;
                    break;
                }
            }
            fclose($handle);
        }
    }

    if ($profileCount == 0) {
        $header = $profileHeader;
    }

    $profileCount++;
}

$countColumns = 0;
if (!empty($profilesTesterResult)) {
    array_unshift($profilesTesterResult, $header);

    $render = '<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Tiki Profiles</title>
    <style>
        table thead th {
            text-align: center;
        }
        table tr td,
        table th {
            border: 1px solid #000;
        }
        table tr td.success {
            color: green;
        }
        table tr td.warning {
            color: orange;
            background-color: #fff3cd;
            cursor: pointer;
        }
        table tr td.error {
            color: red;
            background-color: #f8d7da;
            cursor: pointer;
        }
        .popup-container {
            display: none;
        }
        .details {
            cursor: pointer;
        }
        .close {
            cursor: pointer;
            float: right;
        }
        .result {
            text-align: center;
        }
        .popover {
            max-width: 100% !important;
        }
    </style>
    <script>
        function openPopup(elementId) {
            if (document.getElementById("container-" + elementId).style.display == "block") {
                document.getElementById("container-" + elementId).style.display = "none";
            } else {
                var container = document.getElementsByClassName("popup-container");
                for (var i = 0; i < container.length; i++) {
                    container[i].style.display = "none";
                }
                document.getElementById("container-" + elementId).style.display = "block";
            }
        }
        function closePopup(elementId) {
            var id = elementId.replace("close", "");
            document.getElementById("container" + id).style.display = "none";
        }
    </script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/jquery.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/fontawesome.min.css" rel="stylesheet">
    <link href="css/fontawesome-solid.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Tiki Profiles Tester</h1>
    <p>Test run on: ' . date('Y-m-d H:i:s') . '</p>
    Notes:<br>
    <ul>
        <li>
            <b>default</b>: Profiles installed one by one in a Tiki instance
        </li>
        <li>
            <b>all</b>: Profiles installed all at once in the same Tiki instance
        </li>
        <li>
            More info at <a href="https://profiles.tiki.org/Tiki-Profiles-Tester">profiles.tiki.org/Tiki-Profiles-Tester</a>
        </li>
    </ul>
    
    <table id="profiles-table" class="table table-hover">';

    foreach ($profilesTesterResult as $index => $rows) {
        if ($index == 0) {
            $countColumns = count($rows);
            $render .= '<thead>';
            $render .= '<tr>';
            foreach ($rows as $row) {
                $render .= '<th>' . $row . '</th>';
            }
            $render .= '</tr>';
            $render .= '</thead>';
            $render .= '<tbody>';
        } else {
            $render .= '<tr>';
            $countRows = 0;
            foreach ($rows as $row) {
                if (strpos($row, '<td') !== false) {
                    $render .= $row;
                } else {
                    $render .= '<td>' . $row . '</td>';
                }

                $countRows++;
            }
            if ($countColumns != $countRows) {
                while ($countRows < $countColumns) {
                    $render .= '<td>N/A</td>';
                    $countRows++;
                }
            }

            $render .= '</tr>';
        }
    }

    $render .= ' </tbody>
    </table>
    </div>
    <script>
        var popoverTriggerList = [].slice.call(document.querySelectorAll(\'[data-bs-toggle="popover"]\'));
        var popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
            return new bootstrap.Popover(popoverTriggerEl, {
                container: \'body\'
            });
        });
    </script>
</body>
</html>';
}

$fileOutput = $appPath . '/public/index.html';
file_put_contents($fileOutput, $render);
