# Tiki Profiles Tester

## Installation

The preferred way to install this extension is through composer.
To install Tiki Manager Tester it is required to have [composer](https://getcomposer.org/download/) installed.

```
php composer.phar install
```

## Configuration

To easily configure Tiki-Profiles-Tester application, copy `.env.dist` file to `.env` and insert your configurations for the uncommented (#) entries.

### Version Control System
Tiki Profiles Tester by default uses git and public repository. If you want o use SVN as your default vcs or another repository please add the following lines to your `.env` file.
```
DEFAULT_VCS=svn
GIT_TIKIWIKI_URI=<CUSTOM_GIT_REPOSITORY_URL>
SVN_TIKIWIKI_URI=<CUSTOM_SVN_REPOSITORY_URL>
```

## Execution

To execute the profile test report for a version (example for 22.x):
```
php tiki-profiles-tester.php profiles:test 22.x
```

To build the report webpage:
```
php scripts/build_report_page.php
```

The page will be available in the public folder.


## Profiles List

To update the list of profiles to be tested edit the following file `scripts/profiles.txt`.


## Tests

Result of the tests can be found in [Tiki Profiles Tester result page](https://tikiwiki.gitlab.io/ci-helpers/tiki-profiles-tester/).
