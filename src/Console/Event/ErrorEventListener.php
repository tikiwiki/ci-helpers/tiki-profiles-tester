<?php

declare(strict_types=1);

namespace TikiProfilesTester\Console\Event;

use App\Exception\AppException;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleErrorEvent;
use Symfony\Component\Console\Style\SymfonyStyle;

class ErrorEventListener implements AppEventListener
{
    public function register($dispatcher): void
    {
        $dispatcher->addListener(ConsoleEvents::ERROR, function (ConsoleErrorEvent $event): void {
            $io = new SymfonyStyle($event->getInput(), $event->getOutput());
            if ($event->getError() instanceof AppException) {
                $io->error($event->getError()->getMessage());
                exit(1);
            }
        });
    }
}
