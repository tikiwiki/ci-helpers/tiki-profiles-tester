<?php

declare(strict_types=1);

namespace TikiProfilesTester\Console;

use Psr\Container\ContainerInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Filesystem\Filesystem;

class Application extends BaseApplication
{
    public const CONFIG_DIST_FILENAME = 'config/config.yaml';

    protected $logo = <<<TXT
 _____ _ _    _
|_   _(_) | _(_)
  | | | | |/ / |
  | | | |   <| |
  |_| |_|_|\_\_|
 ____             __ _ _        _____         _
|  _ \ _ __ ___  / _(_) | ___  |_   _|__  ___| |_ ___ _ __
| |_) | '__/ _ \| |_| | |/ _ \   | |/ _ \/ __| __/ _ \ '__|
|  __/| | | (_) |  _| | |  __/   | |  __/\__ \ ||  __/ |
|_|   |_|  \___/|_| |_|_|\___|   |_|\___||___/\__\___|_|
TXT;

    /**
     * @var string
     */
    private $rootDir;

    /**
     * @var ContainerInterface|null
     */
    private $container;

    public function __construct(string $rootDir)
    {
        $this->rootDir = $rootDir;

        $container = $this->getContainer();
        $name = $container->getParameter('app.name');
        $version = $container->getParameter('app.version');

        parent::__construct($name, $version);
    }

    public function getVersion()
    {
        return 'v'.parent::getVersion();
    }

    public function getName(): string
    {
        $name = parent::getName();

        return $this->logo ?? $name;
    }

    public function loadConfiguration($configFile = null)
    {
        if (null !== $this->container) {
            return;
        }

        $this->container = new ContainerBuilder();
        $this->container->setParameter('app.project_dir', $this->rootDir);

        try {
            $loader = new YamlFileLoader($this->container, new FileLocator(\dirname(__DIR__, 2)));
            $loader->load(static::CONFIG_DIST_FILENAME);
        } catch (\Exception $e) {
            throw new \LogicException(\sprintf('Load configuration failed "%s"', $e->getMessage()), $e->getCode(), $e);
        }

        $this->container->compile(true);
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer()
    {
        if (null === $this->container) {
            $this->loadConfiguration(null);
        }

        return $this->container;
    }

    public function getRootDir()
    {
        return $this->rootDir;
    }

    public function getCacheDir()
    {
        return  $this->getContainer()->getParameter('app.cache_dir');
    }

    public function getTempDir()
    {
        return  $this->getContainer()->getParameter('app.temp_dir');
    }

    protected function getDefaultCommands()
    {
        return \array_merge(parent::getDefaultCommands(), [
            new Command\ProfileCheckCommand(),
        ]);
    }
}
