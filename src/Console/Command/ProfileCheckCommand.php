<?php

/**
 * @copyright (c) Copyright by authors of the Tiki Profiles Tester Project. All Rights Reserved.
 *     See copyright.txt for details and a complete list of authors.
 * @licence Licensed under the GNU LESSER GENERAL PUBLIC LICENSE. See LICENSE for details.
 */

namespace TikiProfilesTester\Console\Command;

use Curl\Curl;
use Exception;
use League\Csv\Writer;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;
use wapmorgan\UnifiedArchive\UnifiedArchive;

class ProfileCheckCommand extends BaseCommand
{
    /** @var OutputInterface */
    protected $output;

    protected $webroot;
    protected $branch;
    protected $dbName;
    protected $commit;

    protected function configure()
    {
        $this
            ->setName('profiles:test')
            ->setDescription('Test Tiki profiles installation for specific Tiki version')
            ->addArgument(
                'branch',
                InputArgument::REQUIRED,
                'Instance branch/tag/commit'
            )
            ->addOption(
                'all',
                null,
                InputOption::VALUE_NONE,
                'Test all profiles at once'
            )
            ->addOption(
                'no-cache',
                null,
                InputOption::VALUE_NONE,
                'Do not use existing Tiki archives in cache.'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;

        $version = $input->getArgument('branch');
        if (empty($version)) {
            $this->io->error('Branch cannot be empty.');
            exit(1);
        }

        $name = date('YmdHis', time()) . '_' . $this->parseTikiVersion($version);
        $webroot = ($_ENV['INSTANCES_FOLDER'] ?? \dirname(__DIR__, 3) . '/tmp/instances') . \DIRECTORY_SEPARATOR . $name;
        $dbName = 'p_' . date('Hsi', time()) . '_db';

        $this->branch = $version;
        $this->webroot = $webroot;
        $this->dbName = $dbName;

        $this->createDatabase($dbName);

        $this->io->writeln('<info>Installing Tiki instance...</info>');
        $this->commit = $this->installTiki($version, $webroot, $input->getOption('no-cache'));
        $this->installComposerDependencies($webroot);

        $dbSettings = <<<TXT
<?php
\$db_tiki='mysqli';
\$host_tiki='{$_ENV['DB_HOST']}';
\$user_tiki='{$_ENV['DB_USER']}';
\$pass_tiki='{$_ENV['DB_PASS']}';
\$dbs_tiki='{$dbName}';
\$client_charset = 'utf8mb4';
TXT;

        file_put_contents($webroot . '/db/local.php', $dbSettings);

        $this->installTikiDatabase();
        $this->updateTikiDatabase();
        $this->rebuildIndex();

        try {
            $profilesFile = file_get_contents(dirname(__DIR__, 3) . '/scripts/profiles.txt');
            $profiles = explode(PHP_EOL, $profilesFile);

            $all = $input->getOption('all');
            $this->applyProfiles($profiles, $all);

            $this->dropDatabase($this->dbName);
        } catch (Exception $e) {
            $this->io->error($e->getMessage());
            return 1;
        }

        return 0;
    }

    /**
     * Apply one profiles in different database
     *
     * @param $profiles
     * @param bool $all
     * @throws \League\Csv\CannotInsertRecord
     */
    public function applyProfiles($profiles, $all = false)
    {
        // Apply all profiles in the same DB
        $profileStatus = [];
        $countProfile = 1;

        $revision = substr($this->commit, 0, 7);
        $phpVersion = phpversion();
        $date = date('Y-m-d', time());
        $dateTime = date('YmdHis', time());
        $fileName = sprintf('%s_%sresults_%s', $dateTime, ($all ? 'all_' : ''), $this->branch);

        $reportsFolder = $_ENV['TIKI_PROFILE_REPORT'] ?? \dirname(__DIR__, 3) . '/reports';
        $fileNamePath = $reportsFolder . '/' . $fileName . '.csv';

        $fs = new Filesystem();
        if (!$fs->exists(\dirname($fileNamePath))) {
            $fs->mkdir(\dirname($fileNamePath));
        }

        $writer = Writer::createFromPath($fileNamePath, 'w');
        $writer->insertOne([
            'Versions[vv]',
            'Revision[vv]',
            'phpv',
            'test_date',
            'N',
            'ProfileName_' . $this->branch,
            'Profile Applied',
            'N.Errors',
            'N.Warnings',
            'Details',
        ]);

        $progressBar = new ProgressBar($this->output, count($profiles));
        $progressBar->setFormat('Progress: %current%/%max% - %message%');

        foreach ($profiles as $profile) {
            $progressBar->setMessage(sprintf('Testing %s', $profile));
            $progressBar->advance();
            $this->io->newLine();

            $result = $this->applyProfile($profile);

            $profileStatus = [
                $this->branch,
                $revision,
                $phpVersion,
                $date,
                $countProfile,
                $profile,
                $result['success'] ? 'true' : 'false',
                $result['errors'],
                $result['warnings'],
                $result['details'],
            ];

            $writer->insertOne($profileStatus);

            if (!$all) {
                $this->installTikiDatabase();
                $this->updateTikiDatabase();
                $this->rebuildIndex();
            }

            $countProfile++;
        }

        $progressBar->setMessage('Finished');
        $progressBar->finish();

        $this->io->newLine();
    }

    /**
     * @param $profile
     * @return array
     */
    protected function applyProfile($profile)
    {
        $this->logger->info(sprintf('Applying Profile: %s', $profile));

        $command = [
            \PHP_BINARY,
            '-q',
            '-d memory_limit=256M',
            'console.php',
            'profile:apply',
            $profile
        ];

        $errors = [];
        $warnings = [];
        $profileResult = '';

        try {
            $process = new Process($command, $this->webroot);
            $this->logger->debug('Command: ' . $process->getCommandLine());

            $process->setTimeout(300); //5 minutes
            $process->run();

            $profileResult = trim($process->getOutput());
            $this->logger->debug(sprintf("Command execution output:\n%s", $profileResult));

            // In case of error the script terminates;
            if (strpos($profileResult, 'Profile applied') !== false) {
                $errors = explode(PHP_EOL, $profileResult);
            }

            // Console.php has a custom error handler (it hides PHP Notice or Warning prefixes)
            // Assuming each line is a warning/notice;
            if (strpos($profileResult, 'Profile applied') !== false) {
                $warnings = explode(PHP_EOL, $profileResult);
                array_pop($warnings);
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            $errors[] = 'Command execution failed with expection. Check the build logs.';
        }

        $nErrors = (int) empty($errors); // It is only one error listed when it occurs
        $nWarnings = count($warnings);
        $success = strpos($profileResult, 'Profile applied') !== false;

        $stats = [
            'success' => $success,
            'errors' => $nErrors,
            'warnings' => $nWarnings,
            'details' => $profileResult,
        ];

        $this->logger->info(sprintf("Profile installation stats:\n%s", var_export($stats, true)));

        return $stats;
    }

    /**
     * (Re-)Install Tiki database
     */
    public function installTikiDatabase()
    {
        $command = [
            \PHP_BINARY,
            'console.php',
            'database:install',
            '--force'
        ];

        $process = new Process($command, $this->webroot);
        $process->setTimeout(300); // 5min
        $process->run();

        if (!$process->isSuccessful()) {
            $this->logger->critical($process->getErrorOutput());
        }

        $this->logger->debug('Tiki database re-installation completed.');
    }

    /**
     * Ensure that all updates were made
     */
    public function updateTikiDatabase()
    {
        $command = [
            \PHP_BINARY,
            'console.php',
            'database:update',
        ];

        $process = new Process($command, $this->webroot);
        $process->setTimeout(300); // 5min
        $process->run();

        if (!$process->isSuccessful()) {
            $this->logger->critical($process->getErrorOutput());
        }

        $this->logger->debug('Tiki database is up-to-date.');
    }

    protected function createDatabase($name)
    {
        $pdo = $this->dbConnect();

        $query = <<<SQL
CREATE DATABASE `$name` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
SQL;

        $this->logger->info('Query: ' . $query);

        if ($pdo->exec($query) === false) {
            $this->logger->critical(sprintf('Failed to create database `%`', $name));
        }

        $this->logger->debug(sprintf('Database %s is now created', $name));
    }

    /**
     * Drop database
     *
     * @param $name
     */
    protected function dropDatabase($name)
    {
        $pdo = $this->dbConnect();

        $query = <<<SQL
DROP DATABASE `$name`;
SQL;

        $this->logger->info('Query: ' . $query);
        if ($pdo->exec($query) === false) {
            $this->logger->error(sprintf('Failed to drop database `%`', $name));
            return;
        }

        $this->logger->debug(sprintf('Database %s dropped', $name));
    }

    protected function dbConnect()
    {
        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        ];

        $host = $_ENV['DB_HOST'] ?? 'localhost';
        $user = $_ENV['DB_USER'] ?? 'root';
        $pass = $_ENV['DB_PASS'];
        $port = $_ENV['DB_PORT'] ?? 3306;

        $dsn = "mysql:host=$host;charset=utf8mb4;port=$port";
        try {
            return new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
    }

    protected function installTiki($version, $webroot, $noCache = false)
    {
        $fs = new Filesystem();
        $cacheDir = $this->getApplication()->getCacheDir();
        $tempDir = $this->getApplication()->getTempDir();
        $projectId = $this->getParameter('gitlab.project_id');

        $fs->mkdir($cacheDir);
        $fs->mkdir($tempDir);

        $parsedVersion = $this->parseTikiVersion($version);
        $file = "$cacheDir/tiki-{$parsedVersion}.tar.gz";

        if (!file_exists($file) || $noCache) {
            $url = sprintf(
                "https://gitlab.com/api/v4/projects/%s/repository/archive.tar.gz?sha=%s",
                $projectId,
                $version
            );
            $this->logger->info('Downloading Tiki from server. URL: ' . $url);

            $fs->mkdir(dirname($file));
            $fs->mkdir($webroot);

            $curl = new Curl();
            $file_handle = fopen($file, 'w+');
            $curl->setOpt(CURLOPT_FILE, $file_handle);
            $curl->get($url);
            $curl->setOpt(CURLOPT_FILE, null);
            fclose($file_handle);
            $curl->close();
        }

        $archive = UnifiedArchive::open($file);
        $list = $archive->getFileNames();
        $dir = dirname($list[0]);

        $this->logger->info(sprintf('Extracting file %s', $file));
        $archive->extractFiles($tempDir);

        $fs->rename(rtrim($tempDir, DIRECTORY_SEPARATOR) . '/' . $dir, $webroot, true);

        // Tiki Console bug, where commands are not available without .git/.svn folder
        $fs->mkdir($webroot . '/.git');

        preg_match('/([^\-]+)$/', $dir, $matches);
        return $matches[1] ?? null;
    }

    protected function installComposerDependencies($webroot)
    {
        $command = [
            'sh',
            'setup.sh',
            'composer',
        ];

        $process = new Process($command, $webroot);
        $process->setTimeout(600); // 10min run
        $process->mustRun(function ($type, $data) {
            if ($this->output->isVeryVerbose()) {
                $this->io->write($data);
            }
        });
    }

    /**
     * This is necessary to avoid errors (missing rebuild) when installing profiles
     *
     * @return void
     */
    protected function rebuildIndex()
    {
        $command = [
            \PHP_BINARY,
            'console.php',
            'index:rebuild'
        ];

        $process = new Process($command, $this->webroot);
        $process->setTimeout(300); // 5min
        $process->run();

        if (!$process->isSuccessful()) {
            $this->logger->critical($process->getErrorOutput());
        }

        $this->logger->debug('Tiki search index completed.');
    }

    protected function parseTikiVersion($version)
    {
        return str_replace('/', '_', $version);
    }
}
